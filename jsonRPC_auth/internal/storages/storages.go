package storages

import (
	"go-rpc/jsonRPC_auth/internal/db/adapter"
	"go-rpc/jsonRPC_auth/internal/infrastructure/cache"
	vstorage "go-rpc/jsonRPC_auth/internal/modules/auth/storage"
	ustorage "go-rpc/jsonRPC_auth/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
