package modules

import (
	"go-rpc/jsonRPC_auth/internal/infrastructure/component"
	acontroller "go-rpc/jsonRPC_auth/internal/modules/auth/controller"
	ucontroller "go-rpc/jsonRPC_auth/internal/modules/user/controller"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		Auth: authController,
		User: userController,
	}
}
