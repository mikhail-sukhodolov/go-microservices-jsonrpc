package modules

import (
	"go-rpc/jsonRPC_auth/internal/infrastructure/component"
	aservice "go-rpc/jsonRPC_auth/internal/modules/auth/service"
	uservice "go-rpc/jsonRPC_auth/internal/modules/user/service"
	"go-rpc/jsonRPC_auth/internal/storages"
)

type Services struct {
	User          uservice.Userer
	Auth          aservice.Auther
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, userService uservice.Userer, components *component.Components) *Services {
	//userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
