package modules

import (
	aservice "go-rpc/jsonRPC_gateway/internal/modules/auth/service"
	uservice "go-rpc/jsonRPC_gateway/internal/modules/user/service"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(userService uservice.Userer, authService aservice.Auther) *Services {
	return &Services{
		User: userService,
		Auth: authService,
	}
}
