package service

import (
	"context"
	"go-rpc/jsonRPC_gateway/internal/infrastructure/errors"
	uservice "go-rpc/jsonRPC_gateway/internal/modules/user/service"
	"net/rpc"
)

type AuthServiceJSONRPC struct {
	client *rpc.Client
}

func NewAuthServiceJSONRPC(client *rpc.Client) *AuthServiceJSONRPC {
	u := &AuthServiceJSONRPC{client: client}

	return u
}

func (t *AuthServiceJSONRPC) Register(ctx context.Context, in RegisterIn) RegisterOut {
	var out RegisterOut
	err := t.client.Call("AuthServiceJSONRPC.Register", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}

	return out
}

func (t *AuthServiceJSONRPC) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	var out AuthorizeOut
	err := t.client.Call("AuthServiceJSONRPC.AuthorizeEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}

	return out
}

func (t *AuthServiceJSONRPC) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	var out AuthorizeOut
	err := t.client.Call("AuthServiceJSONRPC.AuthorizeRefresh", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}

	return out
}

func (t *AuthServiceJSONRPC) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	var out AuthorizeOut
	err := t.client.Call("AuthServiceJSONRPC.AuthorizePhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceGeneralErr
	}

	return out
}

func (t *AuthServiceJSONRPC) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	var out SendPhoneCodeOut
	err := t.client.Call("AuthServiceJSONRPC.SendPhoneCode", in, &out)
	if err != nil {
		return SendPhoneCodeOut{}
	}

	return out
}

func (t *AuthServiceJSONRPC) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	var out VerifyEmailOut
	err := t.client.Call("AuthServiceJSONRPC.AuthorizePhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceVerifyErr
	}

	return out
}

func (t *AuthServiceJSONRPC) SetUserer(user uservice.Userer) {
	//TODO implement me
	panic("implement me")
}
